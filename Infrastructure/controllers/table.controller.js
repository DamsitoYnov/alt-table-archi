import TableService from '../../Domain/services/table.service'
import TableRepository from "../repositories/table.repository";
import CommandeRepository from "../repositories/commande.repository";
import ServiceRepository from "../repositories/service.repository";

export default class TableController {
    constructor() {
        this.tableService = new TableService(TableRepository, CommandeRepository, ServiceRepository)
    }
    post = (req, res) => {
        const response = this.tableService.saveTable(req.body);

        if (response.errorMessage != null) {
            res.status(response.code || 412).json({ message: response.errorMessage });
        }
        else {
            res.status(201).json(response);
        }
    }
    setClient = (req, res) => {
        const response = this.tableService.setClient(req.body, req.params.numero);

        if (response.errorMessage != null) {
            res.status(response.code || 412).json({ message: response.errorMessage });
        }
        else {
            res.status(200).json(response);
        }
    }
    end = (req, res) => {
        const response = this.tableService.end(req.params.numero, req.body);

        if (response.errorMessage != null) {
            res.status(response.code || 412).json({ message: response.errorMessage });
        }
        else {
            res.status(200).json(response);
        }
    }
}