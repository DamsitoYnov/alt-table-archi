import TableRepository from "../repositories/table.repository";
import serviceRepository from "../repositories/service.repository";
import serviceService from '../../Domain/services/service.service'

export default class ServiceController {
    constructor() {
        this.serviceService = new serviceService(serviceRepository)
    }

    addService =  (req, res) => {
        const response = this.serviceService.save(new TableRepository);
        if (response.errorMessage != null) {
            res.status(response.code || 412).json({ message: response.errorMessage });
        }
        else {
            res.status(201).json(response);
        }
    }
}