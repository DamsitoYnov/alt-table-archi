import PlatRepository from "../repositories/plat.repository";
import CommandeService from "../../Domain/services/commande.service";
import CommandeRepository from "../repositories/commande.repository";
import TableRepository from "../repositories/table.repository";

export default class CommandeController {
    constructor() {
        this.commandeService = new CommandeService(CommandeRepository, TableRepository, PlatRepository)
    }
    // req.body : numero de table et id du plat et commentaire
    addPlat = (req, res) => {
        const response = this.commandeService.addPlat(req.body, req.params.numero);
        if (response.errorMessage != null) {
            if (response.error) {
                res.status(response.code || 412).json({ message: response.errorMessage, error: response.error });
            } else {
                res.status(response.code || 412).json({ message: response.errorMessage });
            }
        }
        else {
            res.status(200).json(response);
        }
    }
    addNotation = (req, res) => {
        const response = this.commandeService.addNotation(req.body.note, req.params);
        if (response.errorMessage != null) {
            if (response.error) {
                res.status(response.code || 412).json({ message: response.errorMessage, error: response.error });
            } else {
                res.status(response.code || 412).json({ message: response.errorMessage });
            }
        }
        else {
            res.status(200).json(response);
        }
    }
}