import fs from 'fs'
import getServiceFile from "../data/getServiceFile";
const servicesFile = getServiceFile()

export default class ServiceRepository {
    getServiceFromFile = () => {
        let services = []
        try {
            services = JSON.parse(fs.readFileSync(servicesFile));
        } catch (e) { }

        return services
    }

    saveService = (service) => {
        let services = this.getServiceFromFile()
        service.Id = this.getLastId() + 1
        services.push(service)
        fs.writeFileSync(servicesFile, JSON.stringify(services))
        return service;
    }

    getLastId(){
        let services = this.getServiceFromFile()
        if(services.length === 0){
            return 0;
        }
        return Math.max.apply(Math, services.map(function(o) {
            return o.Id; }));
    }

}
