import { mockRequest, mockResponse } from 'jest-mock-req-res'
import serviceController from '../Infrastructure/controllers/service.controller'
import fs from 'fs'
import getServiceFile from "../Infrastructure/data/getServiceFile";
import tableController from "../Infrastructure/controllers/table.controller.js";
const servicesFile = getServiceFile()
let tableControllerTest = new tableController()
let serviceControllerTest = new serviceController()
fs.writeFile(servicesFile, '', function(){})
describe("Post a new Service", () => {

    it('Should create a new Service and return 201 if Service name doesn\'t exist', async () => {
        let tables = [
            { body: { numero: 1, nbPersonMax: 2} },
            { body: { numero: 2, nbPersonMax: 5} },
            { body: { numero: 3, nbPersonMax: 3} },
            { body: { numero: 4, nbPersonMax: 5} },
        ]

        tables.forEach(table => {
            const reqTable = mockRequest(table)
            const resTable = mockResponse()
            tableControllerTest.post(reqTable, resTable)

        })

        // Arrange
        const req = mockRequest()

        const res = mockResponse()
        // Act
        serviceControllerTest.addService(req, res)

        expect(res.status).toHaveBeenCalledWith(201)
    });
});
