import { mockRequest, mockResponse } from 'jest-mock-req-res'
import fs from 'fs'
import getPlatFile from "../Infrastructure/data/getPlatFile";
import PlatController from "../Infrastructure/controllers/plat.controller";
const platsFile = getPlatFile()
let platControllerTest = new PlatController()
fs.writeFile(platsFile, '', function(){})
describe("Post a new Plat", () => {

  it('Should create a new Plat and return 201 if Plat name doesn\'t exist', async () => {
    // Arrange
    const req = mockRequest({ body: { name : "MOJITO", description: "description",
      type: "Apéritif", price: 12.56, quantity: 55} })
    const res = mockResponse()
    // Act
    platControllerTest.post(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(201)
  });
  it('Should create a new Plat and return 201 if Plat name doesn\'t exist with no quantity', async () => {
    // Arrange
    const req = mockRequest({ body: { name : "TEST", description: "description",
        type: "Apéritif", price: 12.56} })
    const res = mockResponse()
    // Act
    platControllerTest.post(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(201)
  });
  it('Should get all plats', async () => {
    // Arrange
    const req = mockRequest()
    const res = mockResponse()
    // Act
    platControllerTest.getAllPlats(req, res)
    // Assert
    expect(res.status).toHaveBeenCalledWith(200)
    expect(res.json).toHaveBeenCalledWith(
        [
          {
            "Id": 1,
            "Name": "MOJITO",
            "Description": "description",
            "Type": "Apéritif",
            "Price": 12.56,
            "Quantity": 55,
          },
          {
            "Id": 2,
            "Name": "TEST",
            "Description": "description",
            "Type": "Apéritif",
            "Price": 12.56,
            "Quantity": 0,
          }
        ]
    )
  });
  it('Should get all plats disponibles', async () => {
    // Arrange
    const req = mockRequest()
    const res = mockResponse()
    // Act
    platControllerTest.getPlatsDisponibles(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(200)
    expect(res.json).toHaveBeenCalledWith(
        [
          {
            "Id": 1,
            "Name": "MOJITO",
            "Description": "description",
            "Type": "Apéritif",
            "Price": 12.56,
            "Quantity": 55,
          }
        ]
    )
  });


  it('Should not create a new Plat and return 412 if Plat name exists', async () => {
    // Arrange
    const req = mockRequest({ body: { name : "MOJITO", description: "description",
        type: "Apéritif", price: 12.00, quantity: 55} })
    const res = mockResponse()
    // Act
    platControllerTest.post(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({ message : "A plat with same name already exists" })
  });

  it('Should not create a new Plat and return 412 if price is not a number', async () => {
    // Arrange
    const req = mockRequest({ body: { name : "MOJITO", description: "description",
        type: "Apéritif", price: 'daedeaz', quantity: 55} })
    const res = mockResponse()
    // Act
    platControllerTest.post(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({ message : "Le prix doit être un nombre supérieur à 0." })
  });
  it('Should not create a new Plat and return 412 if price is a number <= 0', async () => {
    // Arrange
    const req = mockRequest({ body: { name : "MOJITO", description: "description",
        type: "Apéritif", price: 0, quantity: 55} })
    const res = mockResponse()
    // Act
    platControllerTest.post(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({ message : "Le prix doit être un nombre supérieur à 0." })
  });

  it('Should not create a new Plat and return 412 if type is not from accepted types', async () => {
    // Arrange
    const req = mockRequest({ body: { name : "TEST TYPE", description: "description",
        type: "deazdeaz", price: 12.00, quantity: 55} })
    const res = mockResponse()
    // Act
    platControllerTest.post(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({ message : "Le type doit être un des choix proposés." })
  });
  it('Should not create a new Plat and return 412 if quantity is not a number', async () => {
    // Arrange
    const req = mockRequest({ body: { name : "TEST QUANTITY", description: "description",
        type: "Entrée", price: 12.00, quantity: "addaezd"} })
    const res = mockResponse()
    // Act
    platControllerTest.post(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({ message : "La quantité doit être un nombre supérieur ou égal à 0" })
  });

  it('Should not create a new Plat and return 412 if quantity a number < 0', async () => {
    // Arrange
    const req = mockRequest({ body: { name : "TEST QUANTITY", description: "description",
        type: "Entrée", price: 12.00, quantity: -15} })
    const res = mockResponse()
    // Act
    platControllerTest.post(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({ message : "La quantité doit être un nombre supérieur ou égal à 0" })
  });

  it('mettre a jour la quantity d\'un plat dispo', async () => {
    // Arrange
    const req = mockRequest({ body: { quantity: 70}, params: {id: 1} })
    const res = mockResponse()
    // Act
    platControllerTest.putQuantity(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(200)

    expect(res.json).toHaveBeenCalledWith({ id: 1, quantity: 70 })

  });
  it('Message d\'erreur quand on veut mettre a jour la quantity d\'un plat qui n\'existe pas', async () => {
    // Arrange
    const req = mockRequest({ body: { quantity: 70}, params: {id: 10} })
    const res = mockResponse()
    // Act
    platControllerTest.putQuantity(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(404)

    expect(res.json).toHaveBeenCalledWith({
     message: "Le plat n'est pas connu."
    })
  });
  it('Message d\'erreur quand on veut mettre a jour la quantity d\'un plat sans l\'id', async () => {
    // Arrange
    const req = mockRequest({ body: { quantity: 70} })
    const res = mockResponse()
    // Act
    platControllerTest.putQuantity(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(404)

    expect(res.json).toHaveBeenCalledWith({
      message: "L'id du plat doit être renseigné."
    })
  });
});
