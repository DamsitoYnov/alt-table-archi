import createService from "../factories/service.factory.js";

export default class ServiceService {
    constructor(serviceRepository) {
        this.serviceRepository = new serviceRepository
    }

    save = (tableRespository) => {
        let availableTables = tableRespository.getAllTables()

        if (availableTables.length < 0)
            return { errorMessage: "Création de service impossible: pas de tables disponible" };

        return this.serviceRepository.saveService(createService(availableTables));
    }
}