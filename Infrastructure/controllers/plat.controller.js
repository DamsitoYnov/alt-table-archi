import PlatService from '../../Domain/services/plat.service'
import PlatRepository from "../repositories/plat.repository";

export default class PlatController {
    constructor() {
        this.platService = new PlatService(PlatRepository)
    }
    post = (req, res) => {
        const response = this.platService.saveplat(req.body);
        if (response.errorMessage != null) {
            res.status(response.code || 412).json({ message: response.errorMessage });
        }
        else {
            res.status(201).json(response);
        }
    }

    getAllPlats = (req, res) => {
        const response = this.platService.getAllPlats();
        res.status(200).json(response);
    }
    getOne = (req, res) => {
        const response = this.platService.getOne(req.params.id);
        res.status(200).json(response);
    }
    getPlatsDisponibles = (req, res) => {
        const response = this.platService.getPlatsDisponibles();
        res.status(200).json(response);
    }
    putQuantity = (req, res) => {
        let id = req.params.id;
        if (!id) {
            res.status(404).json({ message: "L'id du plat doit être renseigné." });
        }
        const response = this.platService.changeQuantity({ id, quantity: req.body.quantity });
        if (response.errorMessage != null) {
            res.status(response.code || 412).json({ message: response.errorMessage });
        }
        else {
            res.status(200).json(response);
        }
    }
}