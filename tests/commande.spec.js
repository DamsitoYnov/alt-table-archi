import { mockRequest, mockResponse } from 'jest-mock-req-res'
import commandeController from '../Infrastructure/controllers/commande.controller'
import fs from 'fs'
import getCommandeFile from "../Infrastructure/data/getCommandeFile";
import axios from "axios";
import tableController from "../Infrastructure/controllers/table.controller";
import InformationsController from "../Infrastructure/controllers/informations.controller";
const commandesFile = getCommandeFile()
let tableControllerTest = new tableController()
let informationControllerTest = new InformationsController()
let commandeControllerTest = new commandeController()
describe("Post a new Commande", () => {

  it('Add notation to commande 1 and plat ', async () => {
    let plat = await axios.put('http://localhost:3000/tables/1/plats',
      {
          id: 1,
          commentaire: "Album with a Vengeance"
    })
    const req = mockRequest({ body: { note: 1}, params: {idCommande: 1, idPlat: plat.data.id} })
    const res = mockResponse()
    // Act
    commandeControllerTest.addNotation(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(200)
  });

    it('Add notation to commande 1 and plat without id commande ', async () => {
        const req = mockRequest({ body: { note: 1}, params: { idPlat: 1} })
        const res = mockResponse()
        // Act
        commandeControllerTest.addNotation(req, res)

        // Assert
        expect(res.status).toHaveBeenCalledWith(412)
        expect(res.json).toHaveBeenCalledWith({
            message: "L'id de la commande est requis."
        })
    });
    it('Add notation to commande 1 and plat without id plat ', async () => {
        const req = mockRequest({ body: { note: 1}, params: {idCommande: 1} })
        const res = mockResponse()
        // Act
        commandeControllerTest.addNotation(req, res)

        // Assert
        expect(res.status).toHaveBeenCalledWith(412)
        expect(res.json).toHaveBeenCalledWith({
            message: "L'id du plat est requis."
        })
    });

    it('Add notation to commande 1 and plat with note < 0 ', async () => {
        let plat = await axios.put('http://localhost:3000/tables/1/plats',
            {
                id: 1,
                commentaire: "Note < 0"
            })

        const req = mockRequest({ body: { note: -1}, params: {idCommande: 1, idPlat: plat.data.id} })
        const res = mockResponse()
        // Act
        commandeControllerTest.addNotation(req, res)

        // Assert
        expect(res.status).toHaveBeenCalledWith(412)
        expect(res.json).toHaveBeenCalledWith({
            message: "La note doit etre comprise entre 0 et 5"
        })
    });

    it('Add notation to commande 1 and plat with note > 5 ', async () => {
        let plat = await axios.put('http://localhost:3000/tables/1/plats',
            {
                id: 1,
                commentaire: "Note > 5"
            })

        const req = mockRequest({ body: { note: 6}, params: {idCommande: 1, idPlat: plat.data.id} })
        const res = mockResponse()
        // Act
        commandeControllerTest.addNotation(req, res)

        // Assert
        expect(res.status).toHaveBeenCalledWith(412)
        expect(res.json).toHaveBeenCalledWith({
            message: "La note doit etre comprise entre 0 et 5"
        })
    });

    it('end a table with pourboire 45 euros', async () => {
        // Arrange
        const req = mockRequest({ body: { pourboire: 45}, params: {numero: 1} })
        const res = mockResponse()
        tableControllerTest.end(req, res)

        expect(res.status).toHaveBeenCalledWith(200)
    });

    it('Get informations from table', async () => {
        // Arrange
        const req = mockRequest({ params: {numero: 1} })
        const res = mockResponse()
        informationControllerTest.getVueGlobal(req, res)

        expect(res.status).toHaveBeenCalledWith(200)
    });

    it('Should update the table', async () => {
        // Arrange
        const req = mockRequest({ body: { nbPerson: 10}, params: {numero: 1} })
        const res = mockResponse()
        // Act
        tableControllerTest.setClient(req, res)

        // Assert
        expect(res.status).toHaveBeenCalledWith(200)
    });

    it('add plat to table 1', async () => {
        // Arrange
        const req = mockRequest({ body: { id: 1, commentaire: "a point"}, params: {numero: 1} })
        const res = mockResponse()
        commandeControllerTest.addPlat(req, res)
        expect(res.status).toHaveBeenCalledWith(200)
    });
});
