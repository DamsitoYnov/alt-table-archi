
function isPositiveNumber(number){
    return number && !isNaN(number) && number > 0
}

export {isPositiveNumber}