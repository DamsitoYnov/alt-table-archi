import CommandeRepository from "../../Infrastructure/repositories/commande.repository";

export default class InformationsService {
    constructor(CommandeRepository, platRepository, tableRepository) {
        this.commandeRepository = new CommandeRepository
        this.platRepository = new platRepository
        this.tableRepository = new tableRepository
    }

    getVueGlobal = (numero) => {
        let informations = {}
        let table = this.tableRepository.existsByNumero(numero)
        if (table.length === 0)
            return { errorMessage: "La table n'est pas connue." }

        let commande = this.commandeRepository.getCommandeByNumeroTable(table[0].Numero)
        if(!commande){
            informations.etat = "Non démarré"
            return informations
        }
        informations.occupation = table[0].NbPerson ? "Occupée" : "Libre"
        informations.etat = table[0].NbPerson ? "Démarré" : "Terminé"
        informations.price = this.commandeRepository.getAdditionCommandeByNumeroTable(table[0].Numero, this.platRepository)
        informations.details = this.commandeRepository.getDetails(table[0].Numero)
        return informations
    }
}