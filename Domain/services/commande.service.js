export default class CommandeService {
    constructor(CommandeRepository, TableRepository, PlatRepository) {
        this.commandeRepository = new CommandeRepository
        this.tableRepository = new TableRepository
        this.platRepository = new PlatRepository
    }

    addPlat = ({ id, commentaire }, numero) => {
        if (!id) {
            return { errorMessage: "L'id du plat est requis." };
        }
        const plat = this.platRepository.existsByID(id);
        if (plat && plat.length === 0) {
            return { errorMessage: "Ce plat n'existe pas." };
        }
        if (plat[0].Quantity === 0) {
            return { errorMessage: "La quantité de ce plat est de 0.", error: plat[0] };
        }
        if (!numero) {
            return { errorMessage: "Le numéro de table est requis." };
        }
        const table = this.tableRepository.existsByNumero(numero);
        if (table && table.length === 0)
            return { errorMessage: "Cette table n'existe pas.", code: 404 };
        if (!table[0].NbPerson) {
            return { errorMessage: "Il n'est pas possible de commander pour cette table car elle n'est pas installée." };
        }
        this.platRepository.savePlatWithQuantity(id, plat[0].Quantity - 1)
        return this.commandeRepository.addPlat(plat[0], numero, commentaire)
    }
    addNotation = (note, { idCommande, idPlat }) => {
        if (!idCommande) {
            return { errorMessage: "L'id de la commande est requis." };
        }
        if(!this.commandeRepository.find(idCommande)){
            return { errorMessage: "Cette commande n'existe pas." };
        }
        if (!idPlat) {
            return { errorMessage: "L'id du plat est requis." };
        }
        if(!this.commandeRepository.findPlatByIdPlat(idPlat)){
            return { errorMessage: "Ce plat n'existe pas." };
        }
        if (!(note && note >= 0 && note <= 5)) {
            return { errorMessage: "La note doit etre comprise entre 0 et 5" };
        }

        return this.commandeRepository.addNotation({ note, idCommande, idPlat })
    }
}