import fs from 'fs'
import getTableFile from "../data/getTableFile";

const tablesFile = getTableFile()

export default class TableRepository {
  getTableFromFile = () => {
    let tables = []
    try {
      tables = JSON.parse(fs.readFileSync(tablesFile));
    } catch (e) { }

    return tables
  }

  getAllTables = () => {
    return this.getTableFromFile()
  }

  existsByNumero = (numero) => {
    let tables = this.getTableFromFile()
    let table = []
    if (tables && tables.length > 0) {
      table = tables.filter((table) => table.Numero == numero);
    }
    return table;
  }
  end = (numero) => {
    if (!numero) {
      return { errorMessage: "Le numéro de table est requis." };
    }
    let tables = this.getTableFromFile()
    let tableFilter = tables.filter((t) => t.Numero == numero)[0];
    let tablesWithoutTableFilter = tables.filter((t) => t.Numero != numero);
    tableFilter.NbPerson = null
    tableFilter.Arrives_at = null
    tablesWithoutTableFilter.push(tableFilter)
    fs.writeFileSync(tablesFile, JSON.stringify(tablesWithoutTableFilter))
    return tableFilter;

  }
  saveTable = (table) => {
    let tables = this.getTableFromFile()
    tables.push(table)
    fs.writeFileSync(tablesFile, JSON.stringify(tables))
    return table;
  }
  setClient = (table, nbPerson) => {
    let tables = this.getTableFromFile()
    let tableFilter = tables.filter((t) => t.Numero == table.Numero)[0];
    let tablesWithoutTableFilter = tables.filter((t) => t.Numero != table.Numero);
    tableFilter.NbPerson = nbPerson
    tableFilter.Arrives_at = new Date()
    tablesWithoutTableFilter.push(tableFilter)
    fs.writeFileSync(tablesFile, JSON.stringify(tablesWithoutTableFilter))
    return tableFilter;
  }

}
