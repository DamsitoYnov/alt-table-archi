import fs from 'fs'
import getPlatFile from "../data/getPlatFile";

const platsFile = getPlatFile()

export default class PlatRepository {
  getPlatsFromFile = () => {
    let plats = []
    try {
      plats = JSON.parse(fs.readFileSync(platsFile));
    } catch (e) {
    }
    return plats
  }
  existsByName = (platName) => {
    let plats = this.getPlatsFromFile()
    let platWithSameName = []
    if (plats && plats.length > 0) {
      platWithSameName = plats.filter((plat) => plat.Name == platName);
    }
    return platWithSameName.length > 0;
  }
  getAllPlats = () => {
    return this.getPlatsFromFile()
  }

  getOne = (id) => {
    let plats = this.getPlatsFromFile()
    return plats.filter((plat) => plat.Id == id)[0] ?? [];
  }
  getPlatsDisponibles = () => {
    let plats = this.getPlatsFromFile()
    return plats.filter((plat) => plat.Quantity != 0);
  }
  existsByID = (id) => {
    let plats = this.getPlatsFromFile()
    let plat = []
    if (plats && plats.length > 0) {
      plat = plats.filter((plat) => plat.Id == id);
    }
    return plat;
  }
  getLastId(){
    let plats = this.getPlatsFromFile()
    if(plats.length === 0){
      return 0;
    }

    return Math.max.apply(Math, plats.map(function(o) {
      return o.Id; }));
  }

  saveplat = (plat) => {
    let plats = this.getPlatsFromFile()
    plat.Id = this.getLastId() + 1
    plats.push(plat)
    fs.writeFileSync(platsFile, JSON.stringify(plats))
    return plat.Id;
  }

  savePlatWithQuantity = (id, quantity) => {
    let plats = this.getPlatsFromFile()
    let plat = plats.filter((plat) => plat.Id == id)[0];
    let platsWithoutPlat = plats.filter((plat) => plat.Id != id);
    plat.Quantity = quantity
    platsWithoutPlat.push(plat)

    fs.writeFileSync(platsFile, JSON.stringify(platsWithoutPlat))
    return { id: plat.Id, quantity: plat.Quantity };
  }
  getPrice = (platName) => {
    let plats = this.getPlatsFromFile()
    let platWithSameName = []
    if (plats && plats.length > 0) {
      platWithSameName = plats.filter((plat) => plat.Name == platName);
    }
    return platWithSameName[0].Price || null;
  }

}
