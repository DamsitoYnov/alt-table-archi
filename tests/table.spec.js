import { mockRequest, mockResponse } from 'jest-mock-req-res'
import tableController from '../Infrastructure/controllers/table.controller'
import fs from 'fs'
import getTableFile from "../Infrastructure/data/getTableFile";
import CommandeController from "../Infrastructure/controllers/commande.controller";
const tablesFile = getTableFile()
let tableControllerTest = new tableController()
let commandeControllerTest = new CommandeController()
fs.writeFile(tablesFile, '', function(){})
describe("Post a new Table", () => {

  it('Should create a new Table and return 201 if Table name doesn\'t exist', async () => {
    // Arrange
    const req = mockRequest({ body: { numero: 1, nbPersonMax: 20} })
    const res = mockResponse()
    // Act
    tableControllerTest.post(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(201)
  });
  it('Should create a new Table and return 201 if Table name doesn\'t exist', async () => {
    // Arrange
    const req = mockRequest({ body: { numero: 2, nbPersonMax: 10} })
    const res = mockResponse()
    // Act
    tableControllerTest.post(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(201)
  });
  it('add plat to table 1 that isnt installed', async () => {
    // Arrange
    const req = mockRequest({ body: { id: 1, commentaire: "a point"}, params: {numero: 1} })
    const res = mockResponse()
    commandeControllerTest.addPlat(req, res)

    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({
      message: "Il n'est pas possible de commander pour cette table car elle n'est pas installée."
    })
  });

  it('Update: nombre de personnes supérieur', async () => {
    // Arrange
    const req = mockRequest({ body: { nbPerson: 50}, params: {numero:1} })
    const res = mockResponse()
    // Act
    tableControllerTest.setClient(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({
      message: "Le nombre de personnes est supérieur au maximum de personnes autorisées."
    })
  });

  it('Should update the table', async () => {
    // Arrange
    const req = mockRequest({ body: { nbPerson: 5}, params: {numero: 1} })
    const res = mockResponse()
    // Act
    tableControllerTest.setClient(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(200)
  });

  it('Update: numero undefined', async () => {
    // Arrange
    const req = mockRequest({ body: { nbPerson: 5} })
    const res = mockResponse()
    // Act
    tableControllerTest.setClient(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({
      message: "Le numéro de table est requis."
    })
  });

  it('Update: table n\'existe pas', async () => {
    // Arrange
    const req = mockRequest({ body: { nbPerson: 5}, params: {numero: 10} })
    const res = mockResponse()
    // Act
    tableControllerTest.setClient(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(404)
    expect(res.json).toHaveBeenCalledWith({
      message: "Cette table n'existe pas."
    })
  });


  it('Update: test client already installed', async () => {
    // Arrange
    const req = mockRequest({ body: { nbPerson: 5}, params: {numero: 1} })
    const res = mockResponse()
    // Act
    tableControllerTest.setClient(req, res)

    // Assert
    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({
      message: "Des clients sont déjà installés à cette table."
    })
  });

  it('add plat to table 1', async () => {
    // Arrange
    const req = mockRequest({ body: { id: 1, commentaire: "a point"}, params: {numero: 1} })
    const res = mockResponse()
    commandeControllerTest.addPlat(req, res)
    expect(res.status).toHaveBeenCalledWith(200)
  });
  it('add another plat to table 1', async () => {
    // Arrange
    const req = mockRequest({ body: { id: 1, commentaire: "a point"}, params: {numero: 1} })
    const res = mockResponse()
    commandeControllerTest.addPlat(req, res)
    expect(res.status).toHaveBeenCalledWith(200)
  });
  it('add plat to table 1 with plat with quantity to 0', async () => {
    // Arrange
    const req = mockRequest({ body: { id: 2, commentaire: "a point"}, params: {numero: 1} })
    const res = mockResponse()
    commandeControllerTest.addPlat(req, res)
    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({
      message: "La quantité de ce plat est de 0.",
      error: {"Id":2,"Name":"TEST","Description":"description","Type":"Apéritif","Price":12.56,"Quantity":0}
    })
  });
  it('add plat to table 1 without id', async () => {
    // Arrange
    const req = mockRequest({ body: { commentaire: "a point"}, params: {numero: 1} })
    const res = mockResponse()
    commandeControllerTest.addPlat(req, res)

    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({
      message: "L'id du plat est requis."
    })
  });
  it('add plat to table 1 without numero', async () => {
    // Arrange
    const req = mockRequest({ body: { id: 1, commentaire: "a point"} })
    const res = mockResponse()
    commandeControllerTest.addPlat(req, res)

    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({
      message: "Le numéro de table est requis."
    })
  });

  it('add plat to table 1 with a plat that doesnt exist', async () => {
    // Arrange
    const req = mockRequest({ body: { id: 89, commentaire: "a point"} })
    const res = mockResponse()
    commandeControllerTest.addPlat(req, res)

    expect(res.status).toHaveBeenCalledWith(412)
    expect(res.json).toHaveBeenCalledWith({
      message: "Ce plat n'existe pas."
    })
  });
});
