import fs from 'fs'
import getCommandeFile from "../data/getCommandeFile";
import { v4 as uuidv4 } from 'uuid'
const commandesFile = getCommandeFile()

export default class CommandeRepository {
    getCommandeFromFile = () => {
        let commandes = []
        try {
            commandes = JSON.parse(fs.readFileSync(commandesFile));
        } catch (e) { }
        return commandes
    }
    saveCommand = (numero) => {
        let commandes = this.getCommandeFromFile()
        let commande = {}
        commande.Id = this.getLastId() + 1
        commande.Table = numero
        commandes.push(commande)
        fs.writeFileSync(commandesFile, JSON.stringify(commandes))
        return commande.Id;
    }
    addPlat = (plat, numero, commentaire) => {
        let commandes = this.getCommandeFromFile()
        let commandeDeLaTable = this.getLastCommandeByNumero(numero)
        let commandeWithoutTable = commandes.filter((c) => c.Id != this.getLastIdWithNumero(numero)); // commandes sans la commande de la table
        let platName = plat.Name
        let id = uuidv4()
        let platNew = { platName, commentaire, id }
        let platsCommande = commandeDeLaTable.plats || []
        platsCommande.push(platNew)
        commandeDeLaTable.plats = platsCommande
        commandeWithoutTable.push(commandeDeLaTable)
        fs.writeFileSync(commandesFile, JSON.stringify(commandeWithoutTable))
        return platNew
    }
    find(idCommande){
        let commandes = this.getCommandeFromFile()
       return commandes.filter((c) => c.Id == idCommande)[0];// commande de la table
    }

    findPlatByIdPlat(idPlat){
        let commandes = this.getCommandeFromFile()
        let plat = null
        commandes.map(c => {
            c.plats.map(p => {
                if(p.id == idPlat){
                    plat = p
                }
            })
        })
        return plat
    }
    addNotation = ({ note, idCommande, idPlat }) => {
        let commandes = this.getCommandeFromFile()
        let commandeDeLaTable = commandes.filter((c) => c.Id == idCommande)[0];// commande de la table
        let commandeWithoutTable = commandes.filter((c) => c.Id != idCommande); // commandes sans la commande de la table

        let platNew = commandeDeLaTable.plats.filter(p => p.id == idPlat)[0]
        let AllPlatWithoutNew = commandeDeLaTable.plats.filter(p => p.id !== idPlat)
        platNew.Note = note
        AllPlatWithoutNew.push(platNew)
        commandeDeLaTable.plats = AllPlatWithoutNew
        commandeWithoutTable.push(commandeDeLaTable)
        fs.writeFileSync(commandesFile, JSON.stringify(commandeWithoutTable))
        return platNew
    }

    getCommandeByNumeroTable = (numero) => {
        return this.getCommandeFromFile().filter((c) => c.Id == this.getLastIdWithNumero(numero))[0] || null;
    }

    setPourboire = (numeroTable, pourboire) => {
        let commandeDeLaTable = this.getLastCommandeByNumero(numeroTable)
        let commandes = this.getCommandeFromFile()

        let commandeWithoutTable = commandes.filter((c) => c.Id != this.getLastIdWithNumero(numeroTable));

        if(pourboire){
            commandeDeLaTable.pourboire = pourboire
        } else {
            commandeDeLaTable.pourboire = null
        }
        commandeWithoutTable.push(commandeDeLaTable)
        fs.writeFileSync(commandesFile, JSON.stringify(commandeWithoutTable))
        return commandeDeLaTable
    }
    getDetails = (numeroTable) => {
        let commandeDeLaTable = this.getLastCommandeByNumero(numeroTable)
        let nicePlats = commandeDeLaTable.plats.map(p => {
            return {
                name: p.platName,
                commentaire: p.commentaire,
                note: p.Note
            }
        })
        return {plats: nicePlats, pourboire: commandeDeLaTable.pourboire}
    }
    getLastCommandeByNumero(numero){
        let commandes = this.getCommandeFromFile()
        return commandes.filter((c) => c.Id == this.getLastIdWithNumero(numero))[0];
    }
    getLastIdWithNumero(numero){
        let commandes = this.getCommandeFromFile()
        let commandesDeLaTable = commandes.filter((c) => c.Table == numero);
        return Math.max.apply(Math, commandesDeLaTable.map(function(o) {
            return o.Id; }));
    }
    getLastId(){
        let commandes = this.getCommandeFromFile()
        if(commandes.length === 0){
            return 0;
        }
        return Math.max.apply(Math, commandes.map(function(o) {
            return o.Id; }));
    }
   
    getAdditionCommandeByNumeroTable = (numero, platRepository) => {
        let commandeDeLaTable = this.getLastCommandeByNumero(numero)

        let plats = commandeDeLaTable.plats;
        let price = null;
        plats.forEach(p => {
            let platPrice = platRepository.getPrice(p.platName)
            if(!platPrice){
                return;
            }
            price += platPrice
        })
        return price.toFixed(2);
    }

}
