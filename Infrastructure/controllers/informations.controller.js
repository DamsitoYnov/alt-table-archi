import PlatRepository from "../repositories/plat.repository";
import TableRepository from "../repositories/table.repository";
import InformationsService from "../../Domain/services/informations.service";
import CommandeRepository from "../repositories/commande.repository";

export default class InformationsController {
    constructor() {
        this.informationService = new InformationsService(CommandeRepository, PlatRepository, TableRepository)
    }

    getVueGlobal = (req, res) => {
        const response = this.informationService.getVueGlobal(req.params.numero);
        res.status(200).json(response);
    }
}