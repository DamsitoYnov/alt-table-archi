import express from 'express'
import PlatController from "./plat.controller";
import TableController from './table.controller';
import ServiceController from "./service.controller.js";
import CommandeController from "./commande.controller";
import InformationsController from "./informations.controller";

let platController = new PlatController()
let platClientController = new CommandeController()
let tableController = new TableController()
let serviceController = new ServiceController()
let informationsController = new InformationsController()
const PORT = 3000

const app = express()
app.use('/api', express.static('public'));
app.use(express.json())

app.post('/plats', function (req, res){
    platController.post(req, res)
})
app.get('/plats/:id', function (req, res){
    platController.getOne(req, res)
})
app.put('/tables/:numero/plats',function (req, res){
    platClientController.addPlat(req, res)
})

app.get('/plats', function (req, res){
    platController.getAllPlats(req, res)
})
app.get('/plats-disponibles', function (req, res){
    platController.getPlatsDisponibles(req, res)
})
app.patch('/plats/:id/quantity',function (req, res){
    platController.putQuantity(req, res)
})

app.post('/tables', function (req, res){
    tableController.post(req, res)
})
app.patch('/tables/:numero', function (req, res){
    tableController.end(req, res)
})

app.post('/services', function (req, res){
    serviceController.addService(req, res)
})
app.put('/tables/:numero/client', function (req, res){
    tableController.setClient(req, res)
})

app.put('/commande/:idCommande/plat/:idPlat/notation', function (req, res){
    platClientController.addNotation(req, res)
}
)

app.get('tables/:numero/informations', function (req, res){
    informationsController.getVueGlobal(req, res)
})

app.listen(PORT, () => {
    console.log(`Le serveur est lancé sur le port : ${PORT}`)
})