function checkRequire(plat)
{
    return (plat.name && plat.description && plat.type)
}

function checkType(type){
    let accepted = ["Apéritif", "Entrée", "Plat principal", "Dessert", "Boisson"]
    return accepted.includes(type);
}

function checkQuantity(quantity){
    if(quantity){
        return !isNaN(quantity) && quantity >= 0
    }
    return true
}

export {checkRequire, checkType, checkQuantity}