import createPlat from "../factories/plat.factory";
import { checkRequire, checkType, checkQuantity } from "../Utils/checkPlat";
import { isPositiveNumber } from "../Utils/check";

export default class PlatService {
  constructor(platRepository) {
    this.platRepository = new platRepository()
  }

  saveplat = (plat) => {
    if (!checkRequire(plat))
      return { errorMessage: "Les champs sont requis." };

    if (!checkType(plat.type))
      return { errorMessage: "Le type doit être un des choix proposés." };

    if (!isPositiveNumber(plat.price))
      return { errorMessage: "Le prix doit être un nombre supérieur à 0." };

    if (!checkQuantity(plat.quantity))
      return { errorMessage: "La quantité doit être un nombre supérieur ou égal à 0" };

    plat.price.toFixed(2)
    const platWithSameNameExists = this.platRepository.existsByName(plat.name);
    if (platWithSameNameExists)
      return { errorMessage: "A plat with same name already exists" };
    return this.platRepository.saveplat(createPlat(plat));
  }

  changeQuantity = ({ id, quantity }) => {
    let plat = this.platRepository.existsByID(id)
    if (plat.length === 0) {
      return { errorMessage: "Le plat n'est pas connu.", code: 404 };
    }
    return this.platRepository.savePlatWithQuantity(id, quantity)
  }

  getAllPlats = () => {
    return this.platRepository.getAllPlats()
  }

  getOne = (id) => {
    return this.platRepository.getOne(id)
  }

  getPlatsDisponibles = () => {
    return this.platRepository.getPlatsDisponibles()
  }
}