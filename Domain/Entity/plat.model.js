
export default class Plat {
  constructor({ name, description, type, price, quantity = 0 }) {
    this.Id = null;
    this.Name = name;
    this.Description = description;
    this.Type = type;
    this.Price = price;
    this.Quantity = quantity;
  }
}
