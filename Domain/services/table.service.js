import createTable from "../factories/table.factory";
import { isPositiveNumber } from "../Utils/check";
import { checkRequire } from "../Utils/checkTable";

class TableService {
  constructor(tableRepository, commandeRepository, serviceRepository) {
    this.tableRepository = new tableRepository()
    this.commandeRepository = new commandeRepository()
    this.serviceRepository = new serviceRepository()
  }

  saveTable = (table) => {
    if (!checkRequire(table))
      return { errorMessage: "Les champs sont requis." };

    if (!isPositiveNumber(table.nbPersonMax))
      return { errorMessage: "Le nombre de personnes doit être un nombre supérieur à 0." };

    const tableWithSameNumeroExists = this.tableRepository.existsByNumero(table.numero);
    if (tableWithSameNumeroExists && tableWithSameNumeroExists.length !== 0)
      return { errorMessage: "A table with same id already exists" };
    return this.tableRepository.saveTable(createTable(table));
  }

  setClient = ({ nbPerson }, numeroTable) => {
    if (!numeroTable) {
      return { errorMessage: "Le numéro de table est requis." };
    }
    const table = this.tableRepository.existsByNumero(numeroTable);
    if (table && table.length === 0)
      return { errorMessage: "Cette table n'existe pas.", code: 404 };
    if (table[0].NbPerson) {
      return { errorMessage: "Des clients sont déjà installés à cette table." };
    }
    if (nbPerson > table[0].NbPersonMax)
      return { errorMessage: "Le nombre de personnes est supérieur au maximum de personnes autorisées." };
    this.commandeRepository.saveCommand(table[0].Numero)
    return this.tableRepository.setClient(table[0], nbPerson);
  }

  updateTable = (numero, params) => {
    let table = this.tableRepository.existsByNumero(numero)
    if (table.length === 0)
      return { errorMessage: "La table n'est pas connue." }

    let updatedTable = { ...table, params }
    return this.tableRepository.update(updatedTable)

  }
  end = (numero, {pourboire}) => {
    let table = this.tableRepository.existsByNumero(numero)
    if (table.length === 0)
      return { errorMessage: "La table n'est pas connue." }

    if (!table[0].NbPerson) {
      return { errorMessage: "La table n'a été assignée à aucun client." }
    }
    
    this.commandeRepository.setPourboire(table[0].Numero, pourboire)
    return this.tableRepository.end(numero);
  }
  getAllTables = () => {
    return this.tableRepository.getAllTables()
  }
}
export default TableService;